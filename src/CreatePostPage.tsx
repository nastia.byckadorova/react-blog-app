import { Button, Flex, FormControl, FormLabel, Heading, Input, Spacer, Text, Textarea, VStack } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { useMutation, useQueryClient } from 'react-query';
import { useNavigate } from 'react-router-dom';

import { TPost } from './TPost';
import { MAX_POST_BODY_LENGTH } from './constants';
import { createPost } from './postsApi';
import { useDefaultToast } from './useDefaultToast';

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
const hasStringMessage = (obj: any): obj is { message: string } => !!obj && typeof obj.message === 'string';

// eslint-disable-next-line complexity
export const CreatePostPage: React.FC = () => {
  const {
    register,
    // eslint-disable-next-line id-denylist
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Pick<TPost, 'body' | 'title'>>({
    mode: 'onBlur',
    defaultValues: {
      body: '',
      title: '',
    },
  });

  const navigate = useNavigate();
  const showToast = useDefaultToast();

  const queryClient = useQueryClient();
  const createPostMutation = useMutation(createPost, {
    onSuccess: (post) => {
      queryClient.setQueryData(['posts', post.id], post);
      void queryClient.invalidateQueries('posts', { exact: true });

      showToast({
        status: 'success',
        title: 'Post created successfully!',
      });

      navigate(`/posts/${post.id}`);
    },
    onError: (error) => {
      showToast({
        status: 'error',
        title: 'Failed to create post',
        description: hasStringMessage(error) ? error.message : undefined,
      });
    },
  });

  const submitForm = handleSubmit((newPost) => {
    createPostMutation.mutate({
      title: newPost.title,
      body: newPost.body,
    });
  });

  const postBodyLength = watch('body').length;

  return (
    <VStack as="form" align="stretch" px={4} onSubmit={submitForm}>
      <Heading size="md">Create a New Post</Heading>
      <FormControl>
        <FormLabel>Post title</FormLabel>
        <Input {...register('title', { required: true })} isInvalid={!!errors.title} />
        {errors.title?.type === 'required' && <Text color="red.500">This field is required</Text>}
      </FormControl>
      <FormControl>
        <Flex>
          <FormLabel>Post text</FormLabel>
          <Spacer />
          <Text color={postBodyLength > MAX_POST_BODY_LENGTH ? 'red.500' : undefined}>
            {postBodyLength}/{MAX_POST_BODY_LENGTH}
          </Text>
        </Flex>
        <Textarea {...register('body', { required: true, maxLength: MAX_POST_BODY_LENGTH })} isInvalid={!!errors.body}></Textarea>
        {errors.body?.type === 'required' && <Text color="red.500">This field is required</Text>}
        {errors.body?.type === 'maxLength' && <Text color="red.500">Post text cannot exceed {MAX_POST_BODY_LENGTH} characters</Text>}
      </FormControl>

      {!createPostMutation.isLoading ? <Button type="submit">Add Post</Button> : <Button disabled>Adding Post...</Button>}
    </VStack>
  );
};
