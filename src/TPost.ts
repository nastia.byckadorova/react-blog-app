import { TUser } from './TUser';

export type TPost = {
  title: string;
  body: string;
  authorId: string;
  id: string;
  createdAt: string;
};

export type TPostWithAuthor = TPost & { author: TUser };
