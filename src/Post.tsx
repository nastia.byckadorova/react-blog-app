import { Box, Flex, Heading, IconButton, Spacer, Text } from '@chakra-ui/react';
import React from 'react';
import { FaTrashAlt } from 'react-icons/fa';

import { TPostWithAuthor } from './TPost';

type TPostProps = {
  post: TPostWithAuthor;
  onDelete: (event: React.MouseEvent) => void;
};

export const Post: React.FC<TPostProps> = ({ post, onDelete }) => (
  <Flex p={4} shadow="sm" _hover={{ shadow: 'md' }} bg="white" borderRadius="md">
    <Box>
      <Heading size="md" variant="colored">
        {post.title}
      </Heading>
      <Text>Written by {post.author.name}</Text>
    </Box>
    <Spacer />
    <IconButton isRound aria-label="Delete Button" icon={<FaTrashAlt />} variant="ghost" onClick={onDelete} />
  </Flex>
);
