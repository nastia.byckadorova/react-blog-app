import { Box, Heading, VStack } from '@chakra-ui/react';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { Link as RouterLink } from 'react-router-dom';

import { Post } from './Post';
import { TPostWithAuthor } from './TPost';
import { extractErrorMessage } from './extractErrorMessage';
import { getPosts, deletePost } from './postsApi';

export const HomePage: React.FC = () => {
  const { data: posts, isLoading, error } = useQuery('posts', getPosts, { staleTime: 5 * 60 * 1000 });

  const queryClient = useQueryClient();

  const deletePostMutation = useMutation(deletePost, {
    onSuccess: () => void queryClient.invalidateQueries('posts', { exact: true }),
  });

  const errorMessage = extractErrorMessage(error);

  return (
    <>
      {errorMessage && <Heading size="md">{errorMessage}</Heading>}
      {isLoading && <Heading size="md">Posts are loading...</Heading>}
      {posts && <PostList posts={posts} title="All Posts" onPostDelete={deletePostMutation.mutate} />}
    </>
  );
};

type TPostListTypes = {
  posts: TPostWithAuthor[];
  title: string;
  onPostDelete: (postId: string) => void;
};

const PostList: React.FC<TPostListTypes> = ({ posts, title, onPostDelete }) => (
  <Box>
    <Heading size="md" ml={4} mb={4}>
      {title}
    </Heading>
    <VStack spacing={4} align="stretch">
      {posts.map((post) => (
        <RouterLink key={post.id} to={`/posts/${post.id}`}>
          <Post
            post={post}
            onDelete={(event) => {
              event.preventDefault();
              onPostDelete(post.id);
            }}
          />
        </RouterLink>
      ))}
    </VStack>
  </Box>
);
