import { Flex, Heading, Spacer, Link, HStack } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

export const Navbar: React.FC = () => (
  <Flex as="nav">
    <Heading my={4} variant="colored">
      The Bulka&apos;s Blog
    </Heading>
    <Spacer />
    <HStack spacing={4}>
      <Link as={RouterLink} to="/">
        Home
      </Link>
      <Link as={RouterLink} to="/create">
        New Post
      </Link>
    </HStack>
  </Flex>
);
