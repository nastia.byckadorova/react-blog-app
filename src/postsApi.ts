import { TPost, TPostWithAuthor } from './TPost';

export const deletePost = async (id: string) =>
  fetch(`http://localhost:4000/api/posts/${id}`, { method: 'DELETE' }).then(async (response) => {
    if (!response.ok) throw new Error("Couldn't delete post");

    return response.json() as Promise<TPost>;
  });

export const getPosts = async ({ signal }: { signal?: AbortSignal }) =>
  fetch('http://localhost:4000/api/posts/', { signal }).then(async (response) => {
    if (!response.ok) throw new Error("Couldn't get posts");

    return response.json() as Promise<TPostWithAuthor[]>;
  });

export const getPost = async (id: string, { signal }: { signal?: AbortSignal }) =>
  fetch(`http://localhost:4000/api/posts/${id}`, { signal }).then(async (response) => {
    if (!response.ok) throw new Error("Couldn't get post");

    return response.json() as Promise<TPostWithAuthor>;
  });

export const createPost = async (post: Pick<TPost, 'body' | 'title'>) =>
  fetch('http://localhost:4000/api/posts', {
    method: 'POST',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(post),
  }).then(async (response) => {
    if (!response.ok) throw new Error("Couldn't create post");

    return response.json() as Promise<TPostWithAuthor>;
  });

export const updatePost = async (id: string, post: Pick<TPost, 'body' | 'title'>) =>
  fetch(`http://localhost:4000/api/posts/${id}`, {
    method: 'PATCH',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(post),
  }).then(async (response) => {
    if (!response.ok) throw new Error("Couldn't update post");

    return response.json() as Promise<TPostWithAuthor>;
  });
