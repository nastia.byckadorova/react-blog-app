import { Container, Divider } from '@chakra-ui/react';
import { ReactQueryDevtools } from 'react-query/devtools';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import { CreatePostPage } from './CreatePostPage';
import { HomePage } from './HomePage';
import { Navbar } from './Navbar';
import { NotFoundPage } from './NotFoundPage';
import { PostDetailsPage } from './PostDetailsPage';

export const App: React.FC = () => (
  <>
    <Router>
      <Container maxW="container.sm" p={0}>
        <Navbar />
        <Divider mb={4} />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/create" element={<CreatePostPage />} />
          <Route path="/posts/:id" element={<PostDetailsPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Container>
    </Router>
    <ReactQueryDevtools />
  </>
);
