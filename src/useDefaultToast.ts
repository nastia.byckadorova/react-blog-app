import { useToast } from '@chakra-ui/react';

export const useDefaultToast = () =>
  useToast({
    duration: 9000,
    isClosable: true,
  });
